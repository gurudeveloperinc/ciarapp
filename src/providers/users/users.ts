import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {ICustomer} from "../../interfaces/customer.interface";
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import {AlertController} from "ionic-angular";
import {SuperTabsController} from "ionic2-super-tabs";


/*
  Generated class for the UsersProvider provider.

  See https://angular.io/gcide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UsersProvider {

    // url:string = "http://localhost:8000/api/";
    public url:string = "https://ciarapp.gurudeveloperinc.com/api/";

    public user:ICustomer;
    canGoBack:boolean = false;
    previousPage:any;
    currentPage:any = "HomePage";


    constructor(public http: HttpClient,
                public superTabsCtrl: SuperTabsController,
                public alertCtrl: AlertController,
                private transfer: FileTransfer,) {
    }

    changeCurrentPage(page){



        if(page == "HomePage" ||
           page == "ExplorePage" ||
           page == "PostPage" ||
           page == "NotificationsPage" ||
           page == "MoodPage" ){
          this.currentPage = true;
          this.superTabsCtrl.showToolbar(true);
        } else this.superTabsCtrl.showToolbar(false);
    }

    signIn(user) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(user);
        return this.http.post(this.url + "sign-in",body,{headers});
    }

    signUp(user) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(user);
        return this.http.post(this.url + "sign-up",body,{headers});
    }

    signUpUser(user) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(user);
        return this.http.post(this.url + "sign-up-user",body,{headers});
    }


    makePost(){
        let data = {'cid' : this.user.cid};
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "make-post",body,{headers});
    }

    posts(){
        let data = {};
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "posts",body,{headers});
    }




    timeline(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "timeline",body,{headers});
    }


    addCard(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "add-card",body,{headers});
    }

    deleteCard(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "delete-card",body,{headers});
    }

    cards(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "cards",body,{headers});
    }


    changePassword(user) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(user);
        return this.http.post(this.url + "change-password",body,{headers});
    }

    changeUsername(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "change-username",body,{headers});
    }

    checkUsername(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "check-username",body,{headers});
    }

    followers(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "followers",body,{headers});
    }

    follow(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "follow-user",body,{headers});
    }

    unfollow(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "unfollow-user",body,{headers});
    }

    following(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "following",body,{headers});
    }

    like(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "like-post",body,{headers});
    }

    unlike(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "unlike-post",body,{headers});
    }

    makeComment(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "make-comment",body,{headers});
    }

    deleteComment(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "category-posts",body,{headers});
    }

    deletePost(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "delete-post",body,{headers});
    }




    mood(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "mood",body,{headers});
    }

    moodItems(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "mood-items",body,{headers});
    }

    addMood(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "add-mood",body,{headers});
    }

    addMoodPost(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "add-mood-post",body,{headers});
    }

    deleteMood(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "delete-mood",body,{headers});
    }

    deleteMoodPost(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "delete-mood-post",body,{headers});
    }












    editProfile(user) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(user);
        return this.http.post(this.url + "update-profile",body,{headers});
    }

    profile(user) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(user);
        return this.http.post(this.url + "profile",body,{headers});
    }

    events(user) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(user);
        return this.http.post(this.url + "events",body,{headers});
    }

    categories() {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify({});
        return this.http.post(this.url + "categories",body,{headers});
    }

    categoryPosts(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "category-posts",body,{headers});
    }

    updateProfile(){
        this.profile(this.user).subscribe((success:any)=>{
            console.log(success);
            this.user = success.data;
        },(error)=>{console.log(error)});
    }


    search(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "search",body,{headers});
    }

    updateDetails(){
        let data = {'cid' : this.user.cid};
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        this.http.post(this.url + "profile",body,{headers}).subscribe((success:any)=>{
            console.log(success);
            this.user = success.data;
        });
    }

    updateDetailsWithPromise(){
        let data = {'cid' : this.user.cid};
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "profile",body,{headers});
    }

    sendMessage(data){
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "send-message",body,{headers});

    }

    conversation(data){
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "conversation",body,{headers});

    }

    messageUsers(data){
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "message-users",body,{headers});
    }

    setMessageRead(data){
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "set-message-read",body,{headers});
    }

    deleteMessage(data){
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "delete-message",body,{headers});
    }

    deleteConversation(data){
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "delete-conversation",body,{headers});
    }

    upload(images,catid,pid){

        let options: FileUploadOptions = {
            fileKey: 'image',
            fileName: 'upload.jpg',
            params : {
                'pid' : pid,
                'catid' : catid,
            }
        };

        const fileTransfer: FileTransferObject = this.transfer.create();

        for(let i = 0; i < images.length; i++){
            fileTransfer.upload(images[i], this.url + 'post-image', options)
                .then((success:any) => {

                    console.log(success);

                }, (error) => {
                    // error
                    console.log(error);
                })

        }

    }


    showAlert(title,message) {
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: message,
            buttons: ['OK']
        });
        alert.present();
    }



}
