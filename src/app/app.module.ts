import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {IonicApp, IonicModule, IonicErrorHandler, AlertController, ModalController} from 'ionic-angular';
import { MyApp } from './app.component';

import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {IonicStorageModule} from "@ionic/storage";
import {SuperTabsModule} from "ionic2-super-tabs";
import { UsersProvider } from '../providers/users/users';
import {SigninPageModule} from "../pages/signin/signin.module";
import {HttpClientModule} from "@angular/common/http";
import {FileTransfer} from "@ionic-native/file-transfer";
import {LandingPage} from "../pages/landing/landing";
import {MoodPage} from "../pages/mood/mood";
import {PostPage} from "../pages/post/post";
import { Camera } from '@ionic-native/camera';
import {ExplorePage} from "../pages/explore/explore";
import {SignupPageModule} from "../pages/signup/signup.module";
import {ExplorePageModule} from "../pages/explore/explore.module";
import {PostPageModule} from "../pages/post/post.module";
import {MoodPageModule} from "../pages/mood/mood.module";
import {MoodItemsPageModule} from "../pages/mood-items/mood-items.module";
import {LandingPageModule} from "../pages/landing/landing.module";
import {PostDetailsPageModule} from "../pages/post-details/post-details.module";
import {ImagePicker} from "@ionic-native/image-picker";
import {PhotoLibrary} from "@ionic-native/photo-library";

import {SelectMoodPageModule} from "../pages/select-mood/select-mood.module";
import {AddMoodPageModule} from "../pages/add-mood/add-mood.module";
import {ProfilePageModule} from "../pages/profile/profile.module";
import {NotificationsPageModule} from "../pages/notifications/notifications.module";
import {NotificationsPage} from "../pages/notifications/notifications";
import {File} from "@ionic-native/file";
@NgModule({
  declarations: [
      MyApp,
      HomePage,
      TabsPage,
      // LandingPage,
      // MoodPage,
      // PostPage,
      // SearchPage,
      // ExplorePage
  ],
  imports: [
      BrowserModule,
      SigninPageModule,
      PostPageModule,
      MoodPageModule,
      LandingPageModule,
      MoodItemsPageModule,
      ExplorePageModule,
      HttpClientModule,
      ExplorePageModule,
      NotificationsPageModule,
      SignupPageModule,
      SelectMoodPageModule,
      PostDetailsPageModule,
      ProfilePageModule,
      AddMoodPageModule,
      IonicModule.forRoot(MyApp),
      IonicStorageModule.forRoot(),
      SuperTabsModule.forRoot(),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
      MyApp,
      ExplorePage,
      NotificationsPage,
      HomePage,
      TabsPage,
      LandingPage,
      MoodPage,
      PostPage
  ],
  providers: [
      File,
      StatusBar,
      Camera,
      ImagePicker,
      PhotoLibrary,
      SplashScreen,
      FileTransfer,
      AlertController,
      ModalController,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UsersProvider
  ]
})
export class AppModule {}
