export class ICustomer{

    public cid?:number;
    public image:string;
    public username:string;
    public name:string;
    public email:string;
    public password?:string;
    public phone:string;
    public cards:any[];
    public bio?:string;
    public facebook?:string;
    public twitter?:string;
    public instagram?:string;
    public posts:any[];
    public followers:string[];
    public following:string[];


}
