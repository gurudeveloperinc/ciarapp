import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectMoodPage } from './select-mood';

@NgModule({
  declarations: [
    SelectMoodPage,
  ],
  imports: [
    IonicPageModule.forChild(SelectMoodPage),
  ],
})
export class SelectMoodPageModule {}
