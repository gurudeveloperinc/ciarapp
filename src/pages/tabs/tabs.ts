import {Component, ViewChild} from '@angular/core';


import { HomePage } from '../home/home';
import {MoodPage} from "../mood/mood";
import {PostPage} from "../post/post";
import {ExplorePage} from "../explore/explore";
import {UsersProvider} from "../../providers/users/users";
import {ModalController, NavController} from "ionic-angular";
import {ProfilePage} from "../profile/profile";
import {SigninPage} from "../signin/signin";
import {isUndefined} from "ionic-angular/util/util";
import {AddMoodPage} from "../add-mood/add-mood";
import {SuperTabs, SuperTabsController} from "ionic2-super-tabs";
import {NotificationsPage} from "../notifications/notifications";

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {


  selected:number = 0;
  page1 = HomePage;
  page2 = ExplorePage;
  page3 = PostPage;
  page4 = NotificationsPage;
  page5 = MoodPage;


  // @ViewChild(SuperTabs) superTabs: SuperTabs;

  constructor(public navCtrl: NavController,
              public modalCtrl: ModalController,
              private superTabsCtrl: SuperTabsController,
              public userProvider: UsersProvider,) {

      if(isUndefined(this.userProvider.user) || this.userProvider.user == null ) this.navCtrl.setRoot(SigninPage);


  }


  ionViewWillLeave(){

      this.checkBack();
  }


  ionViewDidLoad(){
      // let self = this;
      // let count = 1;
      // setInterval(function(){
      //     self.superTabsCtrl.setBadge('homeTab', count);
      //     count++;
      // },2000);


  }


  checkBack(){
      if(this.navCtrl.canGoBack()) this.userProvider.canGoBack = true;
  }

    goToProfile(){
        this.navCtrl.push(ProfilePage);
    }

    onTabSelect(event:any){
        this.selected = event.index;
    }

    addMood(){
        const modal = this.modalCtrl.create(AddMoodPage);
        modal.present();
    }

    goBack(){
      this.navCtrl.setRoot(TabsPage);
    }

    goToNotifications(){
        // this.navCtrl.push(NotificationsPage);
    }


}
