import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {PostDetailsPage} from "../post-details/post-details";
import {UsersProvider} from "../../providers/users/users";
import {MoodPage} from "../mood/mood";

/**
 * Generated class for the MoodItemsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mood-items',
  templateUrl: 'mood-items.html',
})
export class MoodItemsPage {

    mood:any;
    posts:string[];
    loading:boolean = false;

    constructor(public navCtrl: NavController,
                public alertCtrl: AlertController,
                private userProvider : UsersProvider,
                public navParams: NavParams) {

        this.mood = this.navParams.get('mood');
        this.userProvider.canGoBack = true;
        this.userProvider.previousPage = MoodPage;
    }

    ionViewDidLoad() {
       console.log('ionViewDidLoad MoodItemsPage');
    }


    goToPost(post){
        this.navCtrl.push(PostDetailsPage,{'post' : post})
    }

    deleteMoodPost(post){

        const confirm = this.alertCtrl.create({
            title: 'Confirm',
            message: 'Sure you want to remove this post?',
            buttons: [
                {
                    text: 'No',
                    handler: () => {


                    }
                },
                {
                    text: 'Yes',
                    handler: () => {
                        let data = {
                            'cid' : this.userProvider.user.cid,
                            'pid' : post.pivot.pid,
                            'moid': post.pivot.moid
                        };

                        this.loading = true;
                        this.userProvider.deleteMoodPost(data).subscribe((success:any)=>{
                            if(success.message == "Successful"){
                                this.loading = false;
                                this.getItems();
                                this.userProvider.showAlert("Success","Mood deleted.")
                            } else{
                                this.userProvider.showAlert("Error",success.message);
                            }
                        },(error)=>{
                            this.loading = false;
                            console.log(error);
                        });

                    }
                }
            ]
        });
        confirm.present();
    }


    getItems(){
        let data = {
            "moid" : this.mood.moid
        };

        this.loading = true;
        this.userProvider.moodItems(data).subscribe((success:any)=>{
            this.loading = false;
            if(success.message == "Successful"){
                this.mood = success.data;

            } else{
                this.userProvider.showAlert("Error",success.message);
            }


        },(error)=>{console.log(error)});
    }




}
