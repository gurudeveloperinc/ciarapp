import { Component } from '@angular/core';
import {AlertController, IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {MoodItemsPage} from "../mood-items/mood-items";
import {UsersProvider} from "../../providers/users/users";
import {SelectMoodPage} from "../select-mood/select-mood";
import {AddMoodPage} from "../add-mood/add-mood";
import {isUndefined} from "ionic-angular/es2015/util/util";

/**
 * Generated class for the MoodPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mood',
  templateUrl: 'mood.html',
})
export class MoodPage {

    loading:boolean = false;
    moods:string[];
    refresher:any;


    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public alertCtrl: AlertController,
                public modalCtrl: ModalController,
                private userProvider : UsersProvider) {
    }

    refresh(refresher){
        this.refresher = refresher;
        this.getMoods();
    }


    ionViewDidEnter(){
        this.getMoods();
    }

    ionViewDidLoad() {
    console.log('ionViewDidLoad MoodPage');
    }

    goToDetails(mood){
      this.navCtrl.push(MoodItemsPage,{'mood' : mood});
    }

    getMoods(){
        if(!isUndefined(this.refresher) ) this.refresher.complete();

        let data ={
          'cid' : this.userProvider.user.cid
      };

      this.loading = true;
      this.userProvider.mood(data).subscribe((success:any)=>{
          this.loading = false;

          if(success.message = "Successful"){
              this.moods = success.data;
              console.log(success.data);
          } else {
              this.userProvider.showAlert("Error",success.message);
          }
      },(error)=>{
          console.log(error);

      this.loading = false;
      });
    }

    addMood(){
        const modal = this.modalCtrl.create(AddMoodPage);
        modal.present();
    }

    deleteMood(moid){

        const confirm = this.alertCtrl.create({
            title: 'Confirm',
            message: 'Sure you want to delete this mood collection?',
            buttons: [
                {
                    text: 'No',
                    handler: () => {


                    }
                },
                {
                    text: 'Yes',
                    handler: () => {
                        let data = {
                            'cid' : this.userProvider.user.cid,
                            'moid' : moid
                        };
                        this.loading = true;
                        this.userProvider.deleteMood(data).subscribe((success:any)=>{
                            if(success.message == "Successful"){
                                this.loading = false;
                                this.getMoods();
                                this.userProvider.showAlert("Success","Mood deleted.")
                            } else{
                                this.userProvider.showAlert("Error",success.message);
                            }
                        },(error)=>{
                            this.loading = false;
                            console.log(error);
                        });

                    }
                }
            ]
        });
        confirm.present();
    }

}
