import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {UsersProvider} from "../../providers/users/users";
import {SigninPage} from "../signin/signin";
import {ImagePicker} from "@ionic-native/image-picker";
import {PhotoLibrary} from "@ionic-native/photo-library";
import { Camera, CameraOptions } from '@ionic-native/camera';
import {FileTransfer, FileTransferObject, FileUploadOptions} from '@ionic-native/file-transfer';
import { File } from "@ionic-native/file";

/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

    username:string = 'username';
    name:string = 'test';
    brandName:string = 'testing';
    businessName:string = 'testing';
    phone:string = '67389452323';
    email:string = 'testing@yopmail.com';
    password:string = '12';
    confirmPassword:string = '12';
    categories:string;
    methodOfID:string;
    facebook:string = 'facebook';
    instagram:string = 'insta';
    twitter:string = 'twitter';
    website:string = 'website';
    loading:boolean = false;
    image:string;
    type:string = 'User';

    constructor(public navCtrl: NavController,
                public alertCtrl: AlertController,
                public userProvider : UsersProvider,
                public imagePicker : ImagePicker,
                private camera: Camera,
                private transfer: FileTransfer,
                private file: File,
                private photoLibrary: PhotoLibrary,
                public navParams: NavParams) {
    }

    ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
    }


    signUpSeller(){
        console.log(this.categories);

        if(this.confirmPassword !== this.password) {
            this.password = null;
            this.confirmPassword = null;
            this.showAlert("Error","Passwords don't match. Please try again");
            return;
        }

        this.loading = true;


        const fileTransfer: FileTransferObject = this.transfer.create();


        let options:any = {
            fileKey: "id",
            fileName: "image.jpg",
            chunkedMode: false,
            mimeType: "multipart/form-data",
            params : {
                'username' : this.username,
                'name' : this.name,
                'brandName' : this.brandName,
                'businessName' : this.businessName,
                'phone' : this.phone,
                'email' : this.email,
                'cat' : this.categories,
                'methodOfID' : this.methodOfID,
                'facebook' : this.facebook,
                'instagram' : this.instagram,
                'twitter' : this.twitter,
                'website' : this.website,
                'password' : this.password
            },
        };


        fileTransfer.upload(this.image, this.userProvider.url + 'sign-up' , options)
            .then((response:any) => {
                // success
                let success = JSON.parse(response.response);
                this.loading = false;
                if(success.message == "Successful"){
                    this.showAlert("Success", "Your registration has been sent");
                    this.navCtrl.push(SigninPage);
                }
                else {

                    this.showAlert("Error", success.message);
                }
                console.log(JSON.stringify(success.response));

            }, (error) => {
                this.loading = false;
                console.log(JSON.stringify(error));

            })

    }
    signUp(){

        let data = {
            'username' : this.username,
            'name' : this.name,
            'brandName' : this.brandName,
            'businessName' : this.businessName,
            'phone' : this.phone,
            'email' : this.email,
            'categories' : this.categories,
            'methodOfID' : this.methodOfID,
            'facebook' : this.facebook,
            'instagram' : this.instagram,
            'twitter' : this.twitter,
            'website' : this.website,
            'password' : this.password
        };

        if(this.confirmPassword !== this.password) {
            this.password = null;
            this.confirmPassword = null;
            this.showAlert("Error","Passwords don't match. Please try again");
            return;
        }

        this.loading = true;

        this.userProvider.signUp(data).subscribe((success:any)=>{
            this.loading = false;
            if(success.message == "Successful"){
                this.showAlert("Success", "Your registration has been sent");
                this.navCtrl.push(SigninPage);
            }
            else {
                this.showAlert("Error", success.message);
            }
            console.log(JSON.stringify(success));

        },(error)=>{
            this.loading = false;
            console.log(JSON.stringify(error));
        });

    }

    signUpUser(){

        let data = {
            'username' : this.username,
            'name' : this.name,
            'phone' : this.phone,
            'email' : this.email,
            'password' : this.password
        };

        if(this.confirmPassword !== this.password) {
            this.showAlert("Error","Passwords don't match. Please try again");
            this.password = null;
            this.confirmPassword = null;
            return;
        }

        this.loading = true;

        this.userProvider.signUpUser(data).subscribe((success:any)=>{
            this.loading = false;
            if(success.message == "Successful"){
                this.showAlert("Success", "You have been registered.");
                this.navCtrl.push(SigninPage);
            }
            else {
                this.showAlert("Error", success.message);
            }
            console.log(JSON.stringify(success));

        },(error)=>{
            this.loading = false;
            console.log(JSON.stringify(error));
        });

    }



    removeID(){
        this.image = null;
    }


    takePhoto() {

        const options: CameraOptions = {
            quality: 100,
            saveToPhotoAlbum: false,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true,
            targetWidth: 800,
            targetHeight: 800
        };

        this.camera.getPicture(options).then((imagePath) => {

            // let currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
            // let correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);

            console.log(imagePath);


            this.image = imagePath;



        }, (error) => {
            console.log(error)
            // Handle error
        });
    }

    chooseImage(){

        // let options = {
        //     maximumImagesCount: 1,
        //     width: 800,
        //     height: 800,
        //     quality: 100
        // };
        //
        // this.imagePicker.getPictures(options).then((results) => {
        //
        //     for (let i = 0; i < results.length; i++) {
        //
        //         this.image = results[i];
        //     }
        //
        // }, (err) => {
        //     this.showAlert("Error","Failed to select photo. Please try again.");
        // });



        this.photoLibrary.requestAuthorization().then(() => {
            this.photoLibrary.getLibrary().subscribe({
                next: library => {
                    library.forEach(function(libraryItem) {
                        console.log(libraryItem.id);          // ID of the photo
                        console.log(libraryItem.photoURL);    // Cross-platform access to photo
                        console.log(libraryItem.thumbnailURL);// Cross-platform access to thumbnail
                        console.log(libraryItem.fileName);
                        console.log(libraryItem.width);
                        console.log(libraryItem.height);
                        console.log(libraryItem.creationDate);
                        console.log(libraryItem.latitude);
                        console.log(libraryItem.longitude);
                        console.log(libraryItem.albumIds);    // array of ids of appropriate AlbumItem, only of includeAlbumsData was used
                    });
                },
                error: err => { console.log('could not get photos'); },
                complete: () => { console.log('done getting photos'); }
            });
        })
            .catch(err => console.log('permissions weren\'t granted'));
    }


    showAlert(title,message) {
        this.loading = false;
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: message,
            buttons: ['OK']
        });
        alert.present();
    }

}
