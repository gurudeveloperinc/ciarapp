import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {ICustomer} from "../../interfaces/customer.interface";
import {TabsPage} from "../tabs/tabs";
import {UsersProvider} from "../../providers/users/users";
import {Storage} from "@ionic/storage";

/**
 * Generated class for the SigninPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signin',
  templateUrl: 'signin.html',
})
export class SigninPage {

    email:string = 'promise@gmail.com';
    password:string = '123456';
    loading:boolean = false;


    constructor(public navCtrl: NavController,
                public alertCtrl: AlertController,
                private userProvider: UsersProvider,
                private storage: Storage,
                public navParams: NavParams) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad SigninPage');
    }




    signIn(){

        if(this.checkRequired() == false) return;

        let user = new ICustomer();
        user.email = this.email;
        user.password = this.password;

        this.loading = true;
        this.userProvider.signIn(user).subscribe((success:any)=>{

            if(success.message == "Successful"){
                this.storage.set('isSignedIn',true);
                this.storage.set('user',success.data);
                this.userProvider.user = success.data;
                this.navCtrl.setRoot(TabsPage);
            } else {

                console.log(JSON.stringify(success));
                this.showAlert('Error',success.message);
            }


        },(error)=>{
            console.log(JSON.stringify(error));
            this.showAlert('Error',error.message);
        });

    }

    showAlert(title,message) {
        this.loading = false;
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: message,
            buttons: ['OK']
        });
        alert.present();
    }

    checkRequired(){

        let form = document.getElementById("form");

        for( let i =0; i < form.querySelectorAll("input").length; i++){

            if(form.querySelectorAll("input")[i].required == true &&
                form.querySelectorAll("input")[i].value == ""
            ){

                this.showAlert("Error", form.querySelectorAll("input")[i].name + " is required.");
                return false;
            }

            return true; //all is well. Continue
        }
    }



}
