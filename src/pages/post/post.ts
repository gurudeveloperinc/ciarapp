import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import {FileTransfer, FileTransferObject, FileUploadOptions} from "@ionic-native/file-transfer";
import {UsersProvider} from "../../providers/users/users";
import {HomePage} from "../home/home";

/**
 * Generated class for the PostPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-post',
  templateUrl: 'post.html',
})
export class PostPage {

    images: string[] = [];
    catid : number;
    caption: string;
    pid : number;
    price:number;
    color:string;
    size:string;
    description:string;
    loading : boolean = false;
    showimage:any;
    isProduct:boolean;


    constructor(public navCtrl: NavController,
              private camera: Camera,
                public alertCtrl: AlertController,
                private userProvider : UsersProvider,
                private transfer: FileTransfer,
                public navParams: NavParams) {
    }


    ionViewDidEnter(){
        this.takePhoto();
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad PostPage');
    }


    takePhoto() {

        const options: CameraOptions = {
            quality: 100,
            saveToPhotoAlbum: false,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            // correctOrientation: true,
            targetWidth: 10240,
            targetHeight: 800,
            allowEdit: true
        };

        this.camera.getPicture(options).then((imageData) => {

            // let currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
            // let correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);

            console.log(imageData);
            let base64Image = 'data:image/jpeg;base64,' + imageData;

            this.images.push(base64Image);


        }, (error) => {
            console.log(error)
            // Handle error
        });
    }

    delete(pid){

        let self = this;
        const confirm = this.alertCtrl.create({
            title: 'Confirm',
            message: 'Sure to delete?',
            buttons: [
                {
                    text: 'No',
                    handler: () => {
                        console.log('Disagree clicked');
                    }
                },
                {
                    text: 'Yes',
                    handler: () => {


                        let data = {
                            'cid' : self.userProvider.user.cid,
                            'pid' : pid
                        };

                        self.loading = true;
                        self.userProvider.deletePost(data).subscribe((success)=>{
                            self.userProvider.updateDetails();
                            self.loading = false;
                            self.showAlert("Success","Post Deleted");

                        },(error)=>{
                            console.log(error);
                            self.loading = false;
                        });
                    }
                }
            ]
        });
        confirm.present();


    }


    upload(){

        let productStatus = 1;
        if(!this.isProduct) productStatus = 0;
        this.loading = true;

        let options: FileUploadOptions = {
            fileKey: 'files[]',
            fileName: 'upload.jpg',
            params : {
                'cid' : this.userProvider.user.cid,
                'caption' : this.caption,
                'price' : this.price,
                'color' : this.color,
                'size' : this.size,
                'description' : this.description,
                'isProduct' : productStatus,
            }
        };

        let self = this;

        const fileTransfer: FileTransferObject = this.transfer.create();

        fileTransfer.upload(this.images[0], this.userProvider.url + 'make-post', options)
            .then((success:any) => {

                self.loading = false;
                let data = JSON.parse(success.response);
                console.log(JSON.stringify(data.message));
;
                if(data.message == "Successful"){

                    self.navCtrl.setRoot(HomePage);
                    self.showAlert("Success","Post created.");

                } else {
                    self.showAlert("Error",data.message);

                }

            }, (error) => {

                // error
                console.log(JSON.stringify(error));
                self.loading = false;

            })


    }

    showAlert(title,message) {
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: message,
            buttons: ['OK']
        });
        alert.present();
    }



}
