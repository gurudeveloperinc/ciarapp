import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {UsersProvider} from "../../providers/users/users";
import {PostDetailsPage} from "../post-details/post-details";

/**
 * Generated class for the ExplorePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-explore',
  templateUrl: 'explore.html',
})
export class ExplorePage {

    loading:boolean = false;
    posts:string[];
    searchTerm:string;
    section:string = 'posts';
    events:any[];

    constructor(public navCtrl: NavController,
              private userProvider : UsersProvider,
              public navParams: NavParams) {
    }

    ionViewDidLoad() {
        this.getEvents();
        this.loading = true;
        this.userProvider.posts().subscribe((success:any)=>{
            this.loading = false;
            if(success.message = "Successful"){
                this.posts = success.data;
                console.log(success.data);
            } else {
                this.userProvider.showAlert("Error",success.message);
            }

        },(error)=>{
            this.loading = false;
            console.log(error);
        });

        console.log('ionViewDidLoad ExplorePage');
    }

    getEvents(){
        this.loading = true;
        this.userProvider.events({}).subscribe((success:any)=>{
            this.loading = false;
            if(success.message = "Successful"){
                this.events = success.data;
                console.log(success.data);
            } else {
                this.userProvider.showAlert("Error",success.message);
            }

        },(error)=>{
            this.loading = false;
            console.log(error);
        });

    }


    goToPost(post){
        this.navCtrl.push(PostDetailsPage,{'post' : post})
    }


    search(ev: any) {
        // Reset items back to all of the items
        // this.initializeItems();
        //
        // set val to the value of the searchbar
        // const val = ev.target.value;
        this.searchTerm = ev.target.value;

        let data = {
            'term' : this.searchTerm
        };
        this.loading = true;
        this.userProvider.search(data).subscribe((success:any)=>{
            this.loading = false;
            if(success.message == "Successful"){

                this.posts = success.data;

            } else{
                this.userProvider.showAlert("Error",success.message);
            }

        },(error)=>{console.log(error); this.loading = false;});
        // // if the value is an empty string don't filter the items
        // if (val && val.trim() != '') {
        //     this.items = this.items.filter((item) => {
        //         return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
        //     })
        // }
    }


}
