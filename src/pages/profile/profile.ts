import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {ActionSheetController} from "ionic-angular";
import {ImagePicker} from "@ionic-native/image-picker";
import {UsersProvider} from "../../providers/users/users";
import {FileTransferObject} from "@ionic-native/file-transfer";
import { Camera, CameraOptions } from '@ionic-native/camera';
import {FileTransfer, FileUploadOptions} from "@ionic-native/file-transfer";
import {ICustomer} from "../../interfaces/customer.interface";
import {SigninPage} from "../signin/signin";
import {Storage} from "@ionic/storage";
import {LandingPage} from "../landing/landing";

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {


    isEditingUser:boolean = false;
    isEditingPassword:boolean = false;
    loading:boolean = false;

    currentPassword:string;
    newPassword:string;
    confirmPassword:string;


    username:string;
    name:string;
    email:string;
    password?:string;
    phone:string;
    image:string;
    address:string;
    description:string;
    photoLoading:boolean = false;

    error:string;
    message:string;

    constructor(public navCtrl: NavController,
                private camera : Camera,
                public userProvider: UsersProvider,
                public alertCtrl: AlertController,
                private imagePicker: ImagePicker,
                public transfer: FileTransfer,
                public actionSheetCtrl: ActionSheetController,
                public storage : Storage,
                public navParams: NavParams) {
    }

    ionViewDidEnter() {
        this.userProvider.updateDetails();
    }

    ionViewWillLeave(){
        this.userProvider.canGoBack = false;
        this.userProvider.previousPage = null;
    }

    takePhoto() {

        const options: CameraOptions = {
            quality: 100,
            saveToPhotoAlbum: false,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true,
            targetWidth: 800,
            targetHeight: 800
        };

        this.camera.getPicture(options).then((imagePath) => {

            let currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
            let correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
            const fileTransfer: FileTransferObject = this.transfer.create();


            let option:FileUploadOptions = {
                fileKey: "image",
                fileName: "image.jpg",
                // chunkedMode: false,
                // mimeType: "multipart/form-data",
                params : {uid: this.userProvider.user.cid},
            };

            let self = this;

            self.photoLoading = true;

            fileTransfer.upload(imagePath , this.userProvider.url + 'api/change-profile-photo', option).then(data => {

                self.userProvider.updateDetails();
                self.showAlert("Successful","Your profile image has been updated.");

                self.photoLoading = false;

            }, err => {
                console.log(err);
                self.showAlert("Error","Please try again");

                self.photoLoading = false;
            });

        }, (error) => {
            console.log(error);
            this.showAlert("Error","Please try again");

            this.photoLoading = false;

            // Handle error
        });
    }


    uploadPhoto(){

        let options = {
            maximumImagesCount: 1,
            width: 800,
            height: 800,
            quality: 100
        };

        this.imagePicker.getPictures(options).then((results) => {

            for (var i = 0; i < results.length; i++) {

                let option:FileUploadOptions = {
                    fileKey: "image",
                    fileName: "image.jpg",
                    chunkedMode: false,
                    mimeType: "multipart/form-data",
                    params : {uid: this.userProvider.user.cid},
                };

                const fileTransfer: FileTransferObject = this.transfer.create();


                let self = this;

                self.photoLoading = true;
                fileTransfer.upload(results[i] , this.userProvider.url + 'api/change-profile-photo', option).then(data => {

                    self.showAlert("Successful","Your profile image has been updated.");
                    self.userProvider.updateDetails();
                    self.photoLoading = false;

                }, err => {
                    console.log(err);
                    self.showAlert("Error","Please try again");
                    self.photoLoading = false;
                });

//                this.images.push(results[i]);
            }

        }, (err) => {
            this.showAlert("Error","Failed to select photo. Please try again.");
        });
    }

    photoAction() {

        let actionSheet = this.actionSheetCtrl.create({
            title: 'Please confirm your action',
            buttons: [
                {
                    text: 'Take Photo',
                    handler: () => {
                        this.takePhoto();
                    }
                },{

                    text: 'Upload Photo',
                    handler: () => {
                        this.uploadPhoto();
                    }
                },{
                    text: 'Close Menu',
                    role: 'cancel',
                    handler: () => {
                        //
                    }
                }
            ]
        });
        actionSheet.present();
    }



    editProfile(){

        try{

            // if(!isNullOrUndefined(this.userProvider.user))
                this.isEditingUser = true;

        } catch(e) {}

    }


    save(){

        // if(!Validation.isValidString(this.userProvider.user.username) ){
        //     this.showError("Please fill all fields");
        //     return;
        // }
        //
        // if(!Validation.isValidString(this.userProvider.user.name) ){
        //     this.showError("Please fill all fields");
        //     return;
        // }
        //
        // if(!Validation.isValidNumber(this.userProvider.user.phone) ){
        //     this.showError("Please fill in a valid phone number");
        //     return;
        // }

        let user = new ICustomer();
        user.cid = this.userProvider.user.cid;
        user.username = this.userProvider.user.username;
        user.name= this.userProvider.user.name;
        user.email= this.userProvider.user.email;
        user.phone = this.userProvider.user.phone;
        user.bio = this.userProvider.user.bio;
        user.facebook = this.userProvider.user.facebook;
        user.twitter = this.userProvider.user.twitter;
        user.instagram = this.userProvider.user.instagram;

        this.loading  = true;
        this.userProvider.editProfile(user).subscribe((success:any)=>{
            this.loading = false;
            if(success.message == "Successful"){
                this.cancel();
                this.showAlert("Success","Profile Updated");
                this.userProvider.updateProfile();

            } else{
                this.userProvider.showAlert("Error",success.message);
            }


        },(error)=>{console.log(JSON.stringify(error));
            this.loading = false;
            this.showError("Something went wrong. Please try again");
        });

    }

    changePassword(){
        this.isEditingPassword = true;
    }

    savePassword(){

        // if(Validation.isEmpty(this.currentPassword) ){
        //     this.showError("Please fill all fields");
        //     return;
        // }
        //
        // if(Validation.isEmpty(this.confirmPassword) || Validation.isEmpty(this.newPassword)){
        //     this.showError("Please fill all fields");
        //     return;
        // }

        if(this.confirmPassword != this.newPassword){
            this.showError("Passwords don't match. Please cross-check");
            return;
        }

        try{

            this.loading = true;

            // if(!isNullOrUndefined(this.userProvider.user)){

                let data = {
                    'currentPassword' : this.currentPassword,
                    'newPassword'     : this.newPassword,
                    'cid'            : this.userProvider.user.cid
                };

                this.userProvider.changePassword(data).subscribe((success)=>{

                    this.loading = false;
                    if(success == 1){
                        this.cancel();
                        this.showMessage("Password Changed.");
                    } else {
                        this.showError("Password Change Failed. Please ensure your current password is correct and try again.");
                    }

                }, (error) => {
                    this.showError("Sorry an error occurred. Please try again");
                    console.log(error);
                });

            // }

        } catch(e) {}

    }

    logout(){

        this.storage.set('isSignedIn',false);
        this.storage.set('user',null);
        this.navCtrl.setRoot(LandingPage);
    }

    cancel(){
        this.isEditingUser = false;
        this.isEditingPassword = false;
    }

    delete(pid){

        let self = this;
        const confirm = this.alertCtrl.create({
            title: 'Confirm',
            message: 'Sure to delete?',
            buttons: [
                {
                    text: 'No',
                    handler: () => {
                        console.log('Disagree clicked');
                    }
                },
                {
                    text: 'Yes',
                    handler: () => {


                        let data = {
                            'cid' : self.userProvider.user.cid,
                            'pid' : pid
                        };

                        self.loading = true;
                        self.userProvider.deletePost(data).subscribe((success)=>{
                            self.userProvider.updateDetails();
                            self.loading = false;
                            self.showAlert("Success","Post Deleted");

                        },(error)=>{
                            console.log(error);
                            self.loading = false;
                        });
                    }
                }
            ]
        });
        confirm.present();


    }


    showError(error){
        this.error = error;
        let self = this;
        self.loading=false;
        this.showAlert('Error',error);
        setTimeout(function () {
            self.error = null;
        },5000);
    }

    showMessage(message){
        this.loading = false;
        this.message = message;
        let self = this;
        setTimeout(function () {
            self.message = null;
        },5000);
    }

    showAlert(title,message) {
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: message,
            buttons: ['OK']
        });
        alert.present();
    }



}
