import { Component } from '@angular/core';
import {ModalController, NavController} from 'ionic-angular';
import {UsersProvider} from "../../providers/users/users";
import {PostDetailsPage} from "../post-details/post-details";
import {isUndefined} from "ionic-angular/es2015/util/util";
import {SelectMoodPage} from "../select-mood/select-mood";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

    loading:boolean = false;
    likeLoading:boolean = false;
    posts:any[] = [];
    section:any = "comments";
    moods:string[];


    refresher:any;

    constructor(public navCtrl: NavController,
                public modalCtrl: ModalController,
              private userProvider : UsersProvider,) {

    }

    refresh(refresher){
        this.userProvider.canGoBack = false;
        this.userProvider.previousPage = null;

        this.refresher = refresher;
        this.getPosts();
    }


    ionViewDidEnter(){
        this.userProvider.canGoBack = false;
        this.userProvider.previousPage = null;
        this.getPosts();
    }

    saveToMood(pid) {
        console.log(JSON.stringify(pid));
        const modal = this.modalCtrl.create(SelectMoodPage,{'pid' : pid });
        modal.present();
    }

    goToDetails(post){
        this.userProvider.changeCurrentPage("PostDetailsPage");
        this.navCtrl.push(PostDetailsPage,{'post' : post});
    }

    getPosts(){

        if(!isUndefined(this.refresher) ) this.refresher.complete();

        this.loading = true;

        let data = {
            'cid' : this.userProvider.user.cid
        };

        this.userProvider.timeline(data).subscribe((success:any)=>{

            this.loading = false;
            this.posts = success.data.reverse();

            console.log(JSON.stringify(success.data));

        },(error)=>{
            console.log(JSON.stringify(error));
            this.loading = false;
        });
    }

    like(pid){
        let data = {
            'cid' : this.userProvider.user.cid,
            'pid' : pid
        };

        this.likeLoading = true;
        this.userProvider.like(data).subscribe((success:any)=>{
            this.likeLoading = false;
            if(success.message == "Successful"){
                this.userProvider.showAlert("Success","Liked");
                this.getPosts();
            } else {
                this.userProvider.showAlert("Error",success.message);
            }
        },(error)=>{console.log(error)});
    }

    unlike(pid){
        let data = {
            'cid' : this.userProvider.user.cid,
            'pid' : pid
        };

        this.likeLoading = true;
        this.userProvider.unlike(data).subscribe((success:any)=>{
            this.likeLoading = false;
            if(success.message == "Successful"){
                this.userProvider.showAlert("Success","Un-liked");
                this.getPosts();
            } else {
                this.userProvider.showAlert("Error",success.message);
            }
        },(error)=>{console.log(error)});
    }

    //
    // getMoods(){
    //     let data ={
    //         'cid' : this.userProvider.user.cid
    //     };
    //
    //     this.loading = true;
    //     this.userProvider.mood(data).subscribe((success:any)=>{
    //         this.loading = false;
    //
    //         if(success.message = "Successful"){
    //             this.moods = success.data;
    //             console.log(success.data);
    //         } else {
    //             this.userProvider.showAlert("Error",success.message);
    //         }
    //     },(error)=>{
    //         console.log(error);
    //
    //         this.loading = false;
    //     });
    // }
    //
    // addMoodPost(){
    //     let data = {};
    //
    //     this.userProvider.addMoodPost(data).subscribe((success:any)=>{
    //         if(success.message == "Successful")
    //             this.userProvider.showAlert("Success", "Post added to your mood board.");
    //         else
    //             this.userProvider.showAlert("Error", success.message);
    //
    //     },(error)=>{console.log(error)});
    // }
}
