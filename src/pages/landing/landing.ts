import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {SigninPage} from "../signin/signin";
import {SignupPage} from "../signup/signup";
import {isUndefined} from "ionic-angular/util/util";
import {UsersProvider} from "../../providers/users/users";
import {Storage} from "@ionic/storage";
import {TabsPage} from "../tabs/tabs";

/**
 * Generated class for the LandingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-landing',
  templateUrl: 'landing.html',
})
export class LandingPage {

  constructor(public navCtrl: NavController,
              public userProvider: UsersProvider,
              private storage: Storage,
              public navParams: NavParams) {




      this.storage.get('isSignedIn').then((success:any)=>{
          if(success == true){
              this.storage.get('user').then((success)=>{
                  this.userProvider.user = success;
                  this.navCtrl.setRoot(TabsPage);
              });
          }
      });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LandingPage');
  }

  signIn(){
      this.navCtrl.push(SigninPage);
  }

  signUp(){
      this.navCtrl.push(SignupPage);
  }
}
