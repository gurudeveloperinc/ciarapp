import { Component } from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {SelectMoodPage} from "../select-mood/select-mood";
import {UsersProvider} from "../../providers/users/users";
import {HomePage} from "../home/home";

/**
 * Generated class for the PostDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-post-details',
  templateUrl: 'post-details.html',
})
export class PostDetailsPage {

    section:string = 'comments';
    loading:boolean = false;
    likeLoading:boolean = false;
    post:any;
    comments:string[] = [];
    reviews:string[] = [];
    comment:string;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public modalCtrl: ModalController,
                private userProvider : UsersProvider,
                ) {

      this.post = this.navParams.get('post');
      this.userProvider.canGoBack = true;
      this.userProvider.previousPage = HomePage;
    }

    ionViewDidLoad() {

    console.log('ionViewDidLoad PostDetailsPage');
    }

    saveToMood(pid) {
        console.log(JSON.stringify(pid));
        const modal = this.modalCtrl.create(SelectMoodPage,{'pid' : pid });
        modal.present();
    }


    like(pid){
        let data = {
            'cid' : this.userProvider.user.cid,
            'pid' : pid
        };

        this.likeLoading = true;
        this.userProvider.like(data).subscribe((success:any)=>{
            this.likeLoading = false;
            if(success.message == "Successful"){
                this.post.isLiked = true;
                this.userProvider.showAlert("Success","Liked");

            } else {
                this.userProvider.showAlert("Error",success.message);
            }
        },(error)=>{console.log(error)});
    }


    unlike(pid){
        let data = {
            'cid' : this.userProvider.user.cid,
            'pid' : pid
        };

        this.likeLoading = true;
        this.userProvider.unlike(data).subscribe((success:any)=>{
            this.likeLoading = false;
            if(success.message == "Successful"){
                this.userProvider.showAlert("Success","Un-liked");
                this.post.isLiked = false;
            } else {
                this.userProvider.showAlert("Error",success.message);
            }
        },(error)=>{console.log(error)});
    }


    makeComment(){
        let data = {
            'pid' : this.post.pid,
            'cid' : this.userProvider.user.cid,
            'comment' : this.comment
        }

        this.userProvider.makeComment(data).subscribe((success:any)=>{
            this.loading = false;
            if(success.message == "Successful"){
                this.comments.push(success.data)

            } else{
                this.userProvider.showAlert("Error",success.message);
            }

        },(error)=>{
            this.loading = false;
            console.log(JSON.stringify(error))
        });

    }

}
